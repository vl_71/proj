<?php 
require_once 'db_connect.php';
require_once 'index.php';


$total_pages = $conn->query('SELECT COUNT(*) FROM auto_part')->fetch_row()[0];

$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;
//количество  результатов на странице
$num_results_on_page = 15;

if ($stmt = $conn->prepare('SELECT * FROM auto_part ORDER BY name LIMIT ?,?')) {
	$calc_page = ($page - 1) * $num_results_on_page;
	$stmt->bind_param('ii', $calc_page, $num_results_on_page);
	$stmt->execute(); 
	$result = $stmt->get_result();
}


?>