<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>


	<link rel="stylesheet" href="css/style.css">

</head>

<body>
	<header>

		<form action="search.php" method="post">
			<input class="input_search" type="text" placeholder="search" name="search"><br>
			<input type="submit" class="btn_sbmit">

		</form>
		<?php 
 	require_once 'db_connect.php';


    if (isset($_POST['search'])) {
		$_POST ['search'] = trim ( $_POST ['search']);
	$search = $conn -> real_escape_string($_POST['search']);
	

    $sql_search = "SELECT name FROM auto_part WHERE name LIKE '{$_POST['search']}%' ";
   
    $result_search = $conn->query($sql_search);
    echo "<div class=output >";

    if ($result_search->num_rows > 0 && !empty($_POST['search']) ){
    while($row_search = $result_search->fetch_assoc() ){
        echo "<li class=search_result >$row_search[name]</li>";
    }
    } else {
        echo '<li class="search_result" >0 records</li>';
	}


	
    echo "</div>";
} 

  ?>

	</header>

	<section class="items">

		<?php
require 'pagination.php';
?>

		<?php
  echo '<table border="1">';
echo '<thead>';
echo '<tr>';
echo '<th>id</th>';
echo '<th>name</th>';
echo '<th>price</th>';
echo '<th>quantity</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';?>
		<?php while ($row = $result->fetch_assoc()): 
		  echo "<tr>";
  echo  "<td>$row[id_auto_part]</td>";
  echo  "<td>$row[name]</td>";
  echo  "<td>$row[price]</td>";
  echo  "<td>$row[quantity]</td>";

  echo "</tr>"; ?>
		<?php endwhile; ?>
		</table>
		<?php if (ceil($total_pages / $num_results_on_page) > 0): ?>
		<ul class="pagination">
			<?php if ($page > 1): ?>
			<li class="prev"><a href="pagination.php?page=<?php echo $page-1 ?>">Prev</a></li>
			<?php endif; ?>

			<?php if ($page > 3): ?>
			<li class="start"><a href="pagination.php?page=1">1</a></li>
			<li class="dots">...</li>
			<?php endif; ?>

			<?php if ($page-2 > 0): ?><li class="page"><a
					href="pagination.php?page=<?php echo $page-2 ?>"><?php echo $page-2 ?></a></li><?php endif; ?>
			<?php if ($page-1 > 0): ?><li class="page"><a
					href="pagination.php?page=<?php echo $page-1 ?>"><?php echo $page-1 ?></a></li><?php endif; ?>

			<li class="currentpage"><a href="pagination.php?page=<?php echo $page ?>"><?php echo $page ?></a></li>

			<?php if ($page+1 < ceil($total_pages / $num_results_on_page)+1): ?><li class="page"><a
					href="pagination.php?page=<?php echo $page+1 ?>"><?php echo $page+1 ?></a></li><?php endif; ?>
			<?php if ($page+2 < ceil($total_pages / $num_results_on_page)+1): ?><li class="page"><a
					href="pagination.php?page=<?php echo $page+2 ?>"><?php echo $page+2 ?></a></li><?php endif; ?>

			<?php if ($page < ceil($total_pages / $num_results_on_page)-2): ?>
			<li class="dots">...</li>
			<li class="end"><a
					href="pagination.php?page=<?php echo ceil($total_pages / $num_results_on_page) ?>"><?php echo ceil($total_pages / $num_results_on_page) ?></a>
			</li>
			<?php endif; ?>

			<?php if ($page < ceil($total_pages / $num_results_on_page)): ?>
			<li class="next"><a href="pagination.php?page=<?php echo $page+1 ?>">Next</a></li>
			<?php endif; ?>
		</ul>
		<?php endif; ?>
</body>

</html>
<?php
	$stmt->close();

?>
</section>


</body>

</html>